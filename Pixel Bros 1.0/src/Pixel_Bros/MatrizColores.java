/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Pixel_Bros;

/**
 *
 * @author Sephiroth
 */
public class MatrizColores {
    int Colores[][];
    
    //Crea la matriz de colores en blanco
    public MatrizColores(){
        Colores = new int [20][20];
        for (int m = 0; m < 20; m++) {
            for (int n = 0; n < 20; n++) {
                Colores[m][n] = 0;
            }
        }
        
    }
    
    //
    public void colorEscogido(int color){
        for (int m = 0; m < 20; m++) {
            for (int n = 0; n < 20; n++) {
                Colores[m][n] = color;
            }
        }
    }
    
    //Retorna el Color de la casilla
    public int isColor(int m, int n) {
        return Colores[m][n];
    }

    //Asigna el color a una casilla determinada
    public void setColores(int m,int n, int color) {
        Colores[m][n]= color;
    }
    
}
