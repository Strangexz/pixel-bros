/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Pixel_Bros;

//import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JScrollPane;
import javax.swing.JPanel;
import java.awt.*;
import javax.swing.JTabbedPane;
import java.io.*;
import javax.swing.*;

/**
 *
 * @author Strangexz
 */
public class TablaRecords extends JFrame {
    public TablaRecords() {

         super("Records");
          

        DefaultTableModel myModel = new DefaultTableModel();
        myModel.addColumn("Nombre");
        myModel.addColumn("Dibujo");
        myModel.addColumn("Tiempo");
        //myModel.addColumn("Nombre");
        //myModel.addColumn("Nombre");
       
        JTable table = new JTable(myModel);
        table.setPreferredScrollableViewportSize(new Dimension(500, 150));


        File f;
        FileReader lectorArchivo;
        //BufferedReader br = null;
        try{
            f = new File("Record.txt");
            lectorArchivo = new FileReader(f);
            BufferedReader br = new BufferedReader(new FileReader(f));
            String line = br.readLine();
            
            for(int row = 0; row < 10 ; row++){
                for(int column = 0; column<5 ;column++){
                    while (line != null ){
                        String [] rowfields = line.split("\t");
                        myModel.addRow(rowfields);
                        line = br.readLine();
                    }
                }
            }
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        
        JTabbedPane panelInformacion = new JTabbedPane();
        JPanel panel1 = new JPanel();
        panel1.add( new JScrollPane( table ));
        panelInformacion.addTab( "Registros", null, panel1, "Primer panel" );
        //agregar objeto JTabbedPane al contenedor
        getContentPane().add( panelInformacion );

        setSize( 550, 280 );
        setVisible( true );
    }    
    
}
