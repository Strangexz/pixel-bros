/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Pixel_Bros;

/**
 *
 * @author Sephiroth
 */
public class MatrizVisible {
     
    //atributo
    Boolean Visibles[][];

    //constructor
    public MatrizVisible() {
        Visibles= new Boolean[20][20];
    }

    //Crea la matriz de botones presionados
    public void visibilizar(Boolean valor){
        for (int m = 0; m < 20; m++) {
            for (int n = 0; n < 20; n++) {
                Visibles[m][n] = valor;
            }
        }
    }

    //Retorna si determinada casilla esta presionada
    public boolean isValor(int m, int n) {
        return Visibles[m][n];
    }

    //Muestra u oculta una casilla
    public void setVisible(int m,int n, boolean b) {
        Visibles[m][n] = b;
    }
}
