/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * jifrmArmar.java
 *
 * Created on 01-24-2012, 07:09:04 AM
 */
package Pixel_Bros;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
//import javax.swing.JPanel;

/**
 *
 * @author Sephiroth
 */
public class jifrmArmar extends javax.swing.JInternalFrame {
    
    //javax.swing.JLabel jlbTiempo = new javax.swing.JLabel();
    MatrizVisible Visibles = new MatrizVisible();
    MatrizColores Colores = new MatrizColores();
    MatrizColores ColoresPeq = new MatrizColores();
    JLabel PanelesPeq[][] = new JLabel[20][20];
    JButton Botones[][] = new JButton[20][20];
    JButton BotonesPeq[][] = new JButton[20][20];
    String cadena1 = "", cadena2 = "", cadenaNula = "";
    int m = 0, n = 0, color = 0, seg = 0, min = 0, magenta = 0, cyan = 0, naranja = 0, rosa = 0;
    int blanco = 0, negro = 0, rojo = 0, azul = 0, verde = 0 , amarillo = 0, rS, rM;
    

    /** Creates new form jifrmArmar */
    public jifrmArmar() {
        initComponents();
    } 
   
    //clase <Thread> para el cronómetro
    Thread hilo = new Thread()
    {
        @Override
        public void run(){
            try{
                while(true){
                    if(seg==59) { seg=0; min++; }
                    if(min==59) { min=0;}
                    seg++;
                   jLabel1.setText(min+" : "+seg);
                   hilo.sleep(600);
                }
            } catch (java.lang.InterruptedException ie) { System.out.println(ie.getMessage()); }
        }
    };//fin de la clase <Thread>
    
   
    //Con este metodo pongo los botones que sirven de casillas
    public void ponerBotones(){
        for (int i = 0; i < 20; i++) {
            for (int j = 0; j < 20; j++) {
                Botones[i][j] = new JButton();
                //PanelesPeq[i][j] = new JLabel();
                BotonesPeq[i][j] = new JButton(); 
                jPanel1.add(Botones[i][j]);
                jPanel2.add(BotonesPeq[i][j]);
                //jPanel2.add(PanelesPeq[i][j]);
                Botones[i][j].setBounds(i * 30, j * 30, 35, 35); 
                BotonesPeq[i][j].setBounds(i * 10, j * 10, 15, 15); 
                //PanelesPeq[i][j].setBounds(i * 10, j * 10, 11, 11); 
            }//fin del for
        }//fin del for
    }// fin del Método ponerBotones
    
    //método para cargar un juego
    public void nuevaPartida(){
        color = 0;
        jPanel2.setBackground(Color.WHITE);        
        Visibles.visibilizar(false);
        Visualizar();
        
        for(int i = 0; i < 20; i++){
            for(int j = 0; j < 20; j++){
                cadenaNula += "0";
                cadena1 += "0";
            }
            
        }
    }//fin del método nuevaPartida
    
    //método para habilitar los botones
    public void Habilitar(){
        for(int i = 0; i < 20; i++){
            for(int j = 0; j < 20; j++){
                //creo una lista que capture los eventos de los botones
                Botones[i][j].addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent axion){
                        pulsarBoton(axion.getSource());
                    }
                });
                
            }//fin del for
        }//fin del for
    }//fin del metodo Habilitar
    
    //con este método pinto los botones según el dibujo guardado
    public void Visualizar(){        
            for (int i = 0; i < 20; i++) {
                for (int j = 0; j < 20; j++) {
                   Botones[i][j].setBackground(Color.WHITE);
                   BotonesPeq[i][j].setBackground(Color.WHITE);  
                   //PanelesPeq[i][j].setBackground(Color.WHITE); 
                }//fin del for
            }//fin del for
    }//fin del método Visualizar
    
    //con este método leo y cargo un dibujo guardado
    public void leerDibujo(){
        Archivo dibujoArchivado = new Archivo();
        char[] cadenaChar = new char [400];  
        char[] cadenaChar_2 = new char [400];   
        String cadenaDeColores = "", nuevaCadena = "";
        int k = 0;
        
        //aqui usando el metodo "leeGrafico" de la clase "Archivo" q cree leo el archivo .txt
        cadenaDeColores = dibujoArchivado.leerGrafico(); 
        
        //como ven aqui imprimo el tamaño del string o lo q es lo mismo
        //la cantidad d caracteres q tiene incluyendo los saltos d renglón. Debe ser 421
        System.out.print(cadenaDeColores.length() + "\n");   
        
        System.out.print(cadenaDeColores+ "\n");
        
        //aqui reemplazo todos los saltos de renglon ó mejor dicho los elimino        
        cadenaDeColores = cadenaDeColores.replace("\n", "");
        
        //aqui elimino los espacios existentes en el String, si es q los hay =P
        cadenaDeColores.trim();
        
        //aqui imprimo el nuevo tamaño del string q debe ser 400 pues elimine los
        //salto de linea y los espacios
        System.out.print(cadenaDeColores.length() + "\n");
        System.out.println(cadenaDeColores+ "\n");
        
        //con esto convierto la cadena String a un arreglo de caracateres tipo "char"
        cadenaChar = cadenaDeColores.toCharArray();
        
        //Aqui desordeno la cadena usando el metodo "desordenarString"
        //de la clase "Archivo" q cree antes
        nuevaCadena += dibujoArchivado.desordenarString(cadenaDeColores);
        
        //luego convierto ese String a un arreglo de caracteres tipo "char"
        cadenaChar_2 = nuevaCadena.toCharArray();
        System.out.println(nuevaCadena);
        
        //Aqui pinto cada botón del jpanel pequeño y el grande con los String q lei
        //usando 2 sentencias <for> anidadas
        for(int i = 0; i < 20; i++){
            for(int j = 0; j < 20; j++){
                System.out.print(String.valueOf(cadenaChar[k]));
                //pintando el los botones deñ jpanel pequeño
                switch(cadenaChar[k]){
                    case '0':             
                        ColoresPeq.setColores(j, i, 0);                   
                        BotonesPeq[j][i].setBackground(Color.WHITE);
                        //PanelesPeq[j][i].setBackground(Color.WHITE);
                        blanco++;
                        break;
                    case '1':                   
                        ColoresPeq.setColores(j, i, 1);                        
                        BotonesPeq[j][i].setBackground(Color.BLACK);
                        //PanelesPeq[j][i].setBackground(Color.BLACK);
                        negro++;
                        break;
                    case '2':               
                        ColoresPeq.setColores(j, i, 2);                        
                        BotonesPeq[j][i].setBackground(Color.RED);
                        //PanelesPeq[j][i].setBackground(Color.RED);
                        rojo++;
                        break;
                    case '3':                
                        ColoresPeq.setColores(j, i, 3);                      
                        BotonesPeq[j][i].setBackground(Color.BLUE);
                        //PanelesPeq[j][i].setBackground(Color.BLUE);
                        azul++;
                        break;
                    case '4':
                        ColoresPeq.setColores(j, i, 4);                       
                        BotonesPeq[j][i].setBackground(Color.GREEN);
                        //PanelesPeq[j][i].setBackground(Color.GREEN);
                        verde++;
                        break;
                    case '5':
                        ColoresPeq.setColores(j, i, 5);                        
                        BotonesPeq[j][i].setBackground(Color.YELLOW);
                        //PanelesPeq[j][i].setBackground(Color.YELLOW);
                        amarillo++;
                        break;
                    case '6':
                        ColoresPeq.setColores(j, i, 6);                        
                        BotonesPeq[j][i].setBackground(Color.MAGENTA);
                        //PanelesPeq[j][i].setBackground(Color.YELLOW);
                        magenta++;
                        break;
                    case '7':
                        ColoresPeq.setColores(j, i, 7);                        
                        BotonesPeq[j][i].setBackground(Color.ORANGE);
                        //PanelesPeq[j][i].setBackground(Color.YELLOW);
                        naranja++;
                        break;
                    case '8':
                        ColoresPeq.setColores(j, i, 8); 
                        BotonesPeq[j][i].setBackground(Color.CYAN);
                        cyan++;
                        break;
                    case '9':
                        ColoresPeq.setColores(j, i, 9); 
                        BotonesPeq[j][i].setBackground(Color.PINK);
                        rosa++;
                        break;
                }//fin del switch        
                
                //pintando los botones del jpanel grande
                switch(cadenaChar_2[k]){
                    case '0':             
                        Colores.setColores(j, i, 0);
                        Botones[j][i].setBackground(Color.WHITE);
                        break;
                    case '1':                   
                        Colores.setColores(j, i, 1);
                        Botones[j][i].setBackground(Color.BLACK);
                        break;
                    case '2':               
                        Colores.setColores(j, i, 2);
                        Botones[j][i].setBackground(Color.RED);
                        break;
                    case '3':                
                        Colores.setColores(j, i, 3); 
                        Botones[j][i].setBackground(Color.BLUE);
                        break;
                    case '4':
                        Colores.setColores(j, i, 4); 
                        Botones[j][i].setBackground(Color.GREEN);
                        break;
                    case '5':
                        Colores.setColores(j, i, 5);  
                        Botones[j][i].setBackground(Color.YELLOW);
                        break;
                    case '6':
                        Colores.setColores(j, i, 6); 
                        Botones[j][i].setBackground(Color.MAGENTA);
                        break;
                    case '7':
                        Colores.setColores(j, i, 7); 
                        Botones[j][i].setBackground(Color.ORANGE);
                        break;
                    case '8':
                        Colores.setColores(j, i, 8); 
                        Botones[j][i].setBackground(Color.CYAN);
                        break;
                    case '9':
                        Colores.setColores(j, i, 9); 
                        Botones[j][i].setBackground(Color.PINK);
                        break;
                } //fin del switch               
                k++;
            }//fin del for            
        }//fin del for        
        
        cadena1 = cadenaDeColores;
        System.out.println();
        
    }//fin del método leerDibujo
    
    //método que recibe un parametro tipo "Object" despues de apretar el botón
    public void pulsarBoton(Object obj){
        for (int i = 0; i < 20; i++) {
            for (int j = 0; j < 20; j++) {
                if (obj == Botones[i][j]) {
                    pulsarCasilla(i, j);
                }//fin del if
            }//fin del for
        }//fin del for
    }// fin del método pulsarBoton
    
    //Aqui esta lo feo, con este método determino que botón ya esta apretado para intercambiar colores
    public void pulsarCasilla(int i, int j){
        Record dibujoGuardado = new Record();
        int color_2;
        if (i >= 0 && i < 20 && j >= 0 && j < 20 && Visibles.isValor(i, j) == false) {              
            if(Visibles.isValor(m, n) == true){
                //Aqui entro solo si ya se apreto el botón y si es diferente, intercambiando el color de los botones.
                //Esta el parte jodida, si que cranie esta madre, por sencillita q se vea me costo, como 2 horas de nueronas XP
                color_2 = Colores.isColor(i, j);
                colorearCasillas(color, i ,j);
                colorearCasillas(color_2, m ,n);
                Colores.setColores(m, n, color_2);
                Colores.setColores(i, j, color);
                Visibles.setVisible(m, n, false);
                
                //con este <for> ingreso los cambios hechos a la cadena correspondiente al jpanel grande
                for(int x = 0; x < 20; x++){
                    for(int y = 0; y < 20; y++){
                        cadena2 += String.valueOf(Colores.isColor(y, x));
                    }//fin del for
                }//fin del for    
                
                //aqui determino si las cadenas son iguales o no, osea si ya forme el dibujo o no
                //lo macizo de esto es q lo hace en tiempo de ejecución \m/(>_<)\m/ ¡God, I´m good!
                cadena2.trim();
                if(cadena1.equals(cadena2)){
                    //obviamente si son iguales con el método <stop> le doy camote a la madre esta
                    hilo.stop();                    
                    JOptionPane.showMessageDialog(null, "Juego Completado", "¡Felicidades!", JOptionPane.INFORMATION_MESSAGE);                    
                    if(min < 3){
                        //este <if> me sirve para determinar q el cliente en cuestion termino antes de 
                        //los 3 min. Si no lo hizo, valio madres. No lo guarda y ya.
                        rS = seg;
                        rM = min;
                        JOptionPane.showMessageDialog(null, "Eres Rapido, ahora registra tu record.", "¡Juego Terminado!", JOptionPane.INFORMATION_MESSAGE);                    
                        dibujoGuardado.registrarRecord(rS, rM);
                    }else{
                        JOptionPane.showMessageDialog(null, "Bien Hecho!!! Pero puedes mejorar.", "¡Juego Terminado!", JOptionPane.INFORMATION_MESSAGE);                    
                    }//fin del else
                }//fin del if                
            }//fin del if
            else{
                //a este <else> entro si aún no he terminado el juego, osea q las cadenas aún no son iguales
                color = Colores.isColor(i, j);
                Visibles.setVisible(i, j, true);
                m = i;
                n = j;
            }//fin del else
        }//fin del if 
        
        //inicializo la cadena2 porq si no la madre esta me almacenara todos los colores de cada iteración
        //y obvio eso no jodera el programa. Osea pues nos generara un error.
        cadena2 = "";
    }//fin del Método pulsarCasilla
    
    //este método me sirve para colorear los botones del jpanel grande, cuando hacemos los cambios en le juego
    public void colorearCasillas(int color, int i ,int j){
        switch(color){
            case 0:
                Botones[i][j].setBackground(Color.WHITE);                                
                break;
            case 1:
                Botones[i][j].setBackground(Color.BLACK);                                
                break;
            case 2:
                Botones[i][j].setBackground(Color.RED);                                
                break;
            case 3:
                Botones[i][j].setBackground(Color.BLUE);                               
                break;
            case 4:
                Botones[i][j].setBackground(Color.GREEN);                               
                break;
            case 5:
                Botones[i][j].setBackground(Color.YELLOW);                                
                break;
            case 6:
                Botones[i][j].setBackground(Color.MAGENTA);
                break;
            case 7:
                Botones[i][j].setBackground(Color.ORANGE);
                break;
            case 8:
                Botones[i][j].setBackground(Color.CYAN);
                break;
            case 9:
                Botones[i][j].setBackground(Color.PINK);
                break;
        }//fin del switch         
    }//fin del método colorearCasillas

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jSeparator1 = new javax.swing.JSeparator();
        jButton2 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();

        setClosable(true);
        setIconifiable(true);
        setTitle("Armar");
        setMaximumSize(new java.awt.Dimension(900, 720));
        setMinimumSize(new java.awt.Dimension(900, 720));
        setPreferredSize(new java.awt.Dimension(900, 720));
        addInternalFrameListener(new javax.swing.event.InternalFrameListener() {
            public void internalFrameActivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeactivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeiconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameIconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosed(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosing(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameOpened(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameOpened(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setMaximumSize(new java.awt.Dimension(605, 605));
        jPanel1.setMinimumSize(new java.awt.Dimension(605, 605));
        jPanel1.setName("jPanel1"); // NOI18N
        jPanel1.setPreferredSize(new java.awt.Dimension(605, 605));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 605, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 605, Short.MAX_VALUE)
        );

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setMaximumSize(new java.awt.Dimension(200, 200));
        jPanel2.setMinimumSize(new java.awt.Dimension(200, 200));
        jPanel2.setName("jPanel2"); // NOI18N
        jPanel2.setPreferredSize(new java.awt.Dimension(200, 200));

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 205, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 205, Short.MAX_VALUE)
        );

        jSeparator1.setName("jSeparator1"); // NOI18N

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Ok.gif"))); // NOI18N
        jButton2.setText("Abrir");
        jButton2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jButton2.setName("jbtnAbrir"); // NOI18N
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Goo.gif"))); // NOI18N
        jButton4.setName("jbtnComenzar"); // NOI18N
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        jLabel1.setBackground(new java.awt.Color(51, 255, 255));
        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("0 : 00");
        jLabel1.setName("jlbTiempo"); // NOI18N

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/97392zvlwlt4qyt.gif"))); // NOI18N
        jLabel2.setName("jLabel2");

        jMenuBar1.setName("jMenuBar1"); // NOI18N

        jMenu1.setText("Archivo");
        jMenu1.setName("jMenu1"); // NOI18N

        jMenuItem1.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_A, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem1.setText("Abrir");
        jMenuItem1.setName("jMenuItem1");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem1);

        jMenuItem2.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_G, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem2.setText("Comenzar");
        jMenuItem2.setName("jMenuItem2");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem2);

        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(8, 8, 8)
                        .addComponent(jButton2)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 205, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(34, 34, 34)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(33, 33, 33)
                        .addComponent(jButton4, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(71, 71, 71)
                        .addComponent(jLabel2)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(85, 85, 85)
                                .addComponent(jSeparator1))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jButton2)
                                .addGap(14, 14, 14)
                                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jButton4, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(21, 21, 21)
                                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 205, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(24, 24, 24))))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formInternalFrameOpened(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameOpened
        ponerBotones();       
        nuevaPartida();  
    }//GEN-LAST:event_formInternalFrameOpened

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        //con este <if> chequeo si ya se abrio un juego actualmente. Si ya se abrio me avisa.
        if (cadena1.equals(cadenaNula)){
            //aqui entro solamente si no se ha abierto ningún dibujo.
            leerDibujo();
        }else{
            JOptionPane.showMessageDialog(null, "¡Ya se abrio un juego! \nAbre otra ventana", "¡Atención!", JOptionPane.INFORMATION_MESSAGE);                    
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        // con este <if> chequeo q haya cargado un dibujo. Si no lo hizo me avisa.
        if( cadena1.equals(cadenaNula) == false){
            //entrando al <if> habilito los botones del jpanel grande y comienzo el cronometro
            //con el método <start>
            Habilitar();
            hilo.start();
            jButton4.disable();         
        } else{
            JOptionPane.showMessageDialog(null, "¡Aún no has escogido un dibujo!\nBusca uno presionando el botón \"Abrir\"", "¡Atención!", JOptionPane.INFORMATION_MESSAGE);                    
        }//fin del else                
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        //con este <if> chequeo si ya se abrio un juego actualmente. Si ya se abrio me avisa. 
        if (cadena1.equals(cadenaNula)){
            //aqui entro solamente si no se ha abierto ningún dibujo.
            leerDibujo();
        }else{
            JOptionPane.showMessageDialog(null, "¡Ya se abrio un juego! \nAbre otra ventana", "¡Atención!", JOptionPane.INFORMATION_MESSAGE);                    
        }
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        // con este <if> chequeo q haya cargado un dibujo. Si no lo hizo me avisa.
        if( cadena1.equals(cadenaNula) == false){
            //entrando al <if> habilito los botones del jpanel grande y comienzo el cronometro
            //con el método <start>
            Habilitar();
            hilo.start();
            jButton4.disable();         
        } else{
            JOptionPane.showMessageDialog(null, "¡Aún no has escogido un dibujo!\nBusca uno presionando el botón \"Abrir\"", "¡Atención!", JOptionPane.INFORMATION_MESSAGE);                    
        }//fin del else   
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton4;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JSeparator jSeparator1;
    // End of variables declaration//GEN-END:variables
}
