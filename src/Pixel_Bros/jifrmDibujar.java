/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * jifrmDibujar.java
 *
 * Created on 01-24-2012, 05:42:59 AM
 */
package Pixel_Bros;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JOptionPane;
    
/**
 *
 * @author Sephiroth
 */
public class jifrmDibujar extends javax.swing.JInternalFrame {

    MatrizVisible Visibles = new MatrizVisible();
    MatrizColores Colores = new MatrizColores();
    JButton Botones[][] = new JButton[20][20];
    int color = 1;
    
    /** Creates new form jifrmDibujar */
    public jifrmDibujar() {
        initComponents();
    }  
    
    //con este metodo relleno el jpanel con botones
    public void ponerBotones(){
        for (int i = 0; i < 20; i++) {
            for (int j = 0; j < 20; j++) {
                Botones[i][j] = new JButton();
                jPanel1.add(Botones[i][j]);
                Botones[i][j].setBounds(i * 30, j * 30, 34, 34);
                
                //creo una lista que capture los eventos, osea, las veces q apretamos uno de los botones
                Botones[i][j].addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e){
                        pulsarBoton(e.getSource());
                    }
                });
            }//fin del for
        }//fin del for
    }// fin del Método ponerBotones
    
    //método para iniciar el juego
    public void nuevaPartida(){
        color = 1;
        jPanel2.setBackground(Color.BLACK);
        Visibles.visibilizar(false);
        jButton8.setBackground(Color.BLACK);
        jButton4.setBackground(Color.RED);
        jButton5.setBackground(Color.BLUE);
        jButton6.setBackground(Color.GREEN);
        jButton7.setBackground(Color.YELLOW);
        jButton9.setBackground(Color.MAGENTA);
        jButton10.setBackground(Color.ORANGE);
        jButton11.setBackground(Color.CYAN);
        jButton12.setBackground(Color.PINK);
        Visualizar();
    }//fin del método nuevaPartida
    
    //con este método logramos ver q botnoes estan apretados y cuales no para asi colorearlos 
    public void Visualizar(){        
            for (int i = 0; i < 20; i++) {
                for (int j = 0; j < 20; j++) {
                    if (Visibles.isValor(i, j)) {
                        colorearCasillas(color);
                    }//fin del if
                    else{
                        Botones[i][j].setBackground(Color.WHITE);
                        Colores.setColores(i, j, 0);
                    }//fin del else
                }//fin del for
            }//fin del for
    }//fin del método Visualizar
    
    //aqui sabemos q botón a sido presionado
    public void pulsarBoton(Object obj){
        for (int i = 0; i < 20; i++) {
            for (int j = 0; j < 20; j++) {
                if (obj == Botones[i][j]) {
                    pulsarCasilla(i, j);
                }//fin del if
            }//fin del for
        }//fin del for
    }// fin del método pulsarBoton
    
    //pos aqui al saber q botón se presiono, lo cambiamos de color, dependiendo el q escogimos
    public void pulsarCasilla(int i, int j){
        if (i >= 0 && i < 20 && j >= 0 && j < 20 && Visibles.isValor(i, j) == false) {
            Visibles.setVisible(i, j, true);
            Colores.setColores(i, j, color);
        }//fin del if
        else{
            Visibles.setVisible(i, j, false);      
        }//fin del else
        
        //aqui mandamos los cambios y lo vemos en el jpanel
        Visualizar();    
    }//fin del Método pilsarCasilla
    
    //con este método guardamos el dibujo usando el método "crearTxt" de la clase "Archivo" q cree
    public void guardarDibujo(){
        Archivo dibujoArchivado = new Archivo();
        String elegidos = "";
        
        int guardar = JOptionPane.showConfirmDialog(null, "¿Desea Guardar el Dibujo?", "Guardad", JOptionPane.INFORMATION_MESSAGE);                 
        if (JOptionPane.OK_OPTION == guardar){
            String nombre = JOptionPane.showInputDialog(null, "¿Qué nombre desea ponerle al dibujo?");
            for(int i = 0; i < 20; i++){
                for(int j = 0; j < 20; j++){              
                    elegidos += Colores.isColor(j, i);                
                    if(j == 19){                    
                        elegidos += "\n";
                    }//fin del if          
                }//fin del for            
            }//fin del for
            
            dibujoArchivado.crearTxt(nombre+".txt", elegidos); 
  
            JOptionPane.showMessageDialog(null, "Guardado", "¡Proceso Exitoso!", JOptionPane.INFORMATION_MESSAGE);
        }//fin del if        
    }//fin del metodo guardarDibujo
    
    public void elegirColor(String colorElegido){      
        if ("Negro".equals(colorElegido)){
            color = 1;
            colorearCasillas(color);
            jPanel2.setBackground(Color.BLACK);
        }
        
        if ("Rojo".equals(colorElegido)){
            color = 2;
            colorearCasillas(color);
            jPanel2.setBackground(Color.RED);
        }
        
        if ("Azul".equals(colorElegido)){
            color = 3;
            colorearCasillas(color);
            jPanel2.setBackground(Color.BLUE);
        }
        
        if ("Verde".equals(colorElegido)){
            color = 4;
            colorearCasillas(color);
            jPanel2.setBackground(Color.GREEN);
        }
        
        if ("Amarillo".equals(colorElegido)){
            color = 5;
            colorearCasillas(color);
            jPanel2.setBackground(Color.YELLOW);
        }
        
        if ("Magenta".equals(colorElegido)){
            color = 6;
            colorearCasillas(color);
            jPanel2.setBackground(Color.MAGENTA);
        }
        
        if ("Naranja".equals(colorElegido)){
            color = 7;
            colorearCasillas(color);
            jPanel2.setBackground(Color.ORANGE);
        }    
        
        if ("Cyan".equals(colorElegido)){
            color = 8;
            colorearCasillas(color);
            jPanel2.setBackground(Color.CYAN);
        }    
        
        if ("Rosa".equals(colorElegido)){
            color = 9;
            colorearCasillas(color);
            jPanel2.setBackground(Color.PINK);            
        }         
    }//fin del método elegirColor
    
    public void colorearCasillas(int color){
        for (int i = 0; i < 20; i++) {
                for (int j = 0; j < 20; j++) {
                    if (Visibles.isValor(i, j)) {                        
                        switch(Colores.isColor(i, j)){
                            case 1:
                                Botones[i][j].setBackground(Color.BLACK);
                                break;
                            case 2:
                                Botones[i][j].setBackground(Color.RED);
                                break;
                            case 3:
                                Botones[i][j].setBackground(Color.BLUE);
                                break;
                            case 4:
                                Botones[i][j].setBackground(Color.GREEN);
                                break;
                            case 5:
                                Botones[i][j].setBackground(Color.YELLOW);
                                break;
                            case 6:
                                Botones[i][j].setBackground(Color.MAGENTA);
                                break;
                            case 7:
                                Botones[i][j].setBackground(Color.ORANGE);
                                break;
                            case 8:
                                Botones[i][j].setBackground(Color.CYAN);
                                break;
                            case 9:
                                Botones[i][j].setBackground(Color.PINK);
                                break;
                        }//fin del switch                                
                    }else{                        
                        Botones[i][j].setBackground(Color.WHITE);
                    }//fin del else
                }//fin del if
            }//fin del for
    }//fin del método colorearCasillas
    
    public void leerDibujo(){
        Archivo dibujoArchivado = new Archivo();
        char[] cadenaChar = new char [400];
        String cadenaDeColores = "";
        int k = 0;
        
        //aqui usando el metodo "leeGrafico" de la clase "Archivo" q cree leo el archivo .txt
        cadenaDeColores = dibujoArchivado.leerGrafico(); 
        
        //aqui reemplazo todos los saltos de renglon ó mejor dicho los elimino        
        cadenaDeColores = cadenaDeColores.replace("\n", "");
        
        //aqui elimino los espacios existentes en el String, si es q los hay =P
        cadenaDeColores.trim();
        
       //con esto convierto la cadena String a un arreglo de caracateres tipo "char"
        cadenaChar = cadenaDeColores.toCharArray();
        
        //Aqui pinto cada botón del jpanel pequeño y el grande con los String q lei
        //usando 2 sentencias <for> anidadas
        for(int i = 0; i < 20; i++){
            for(int j = 0; j < 20; j++){
                System.out.print(String.valueOf(cadenaChar[k]));
                
                //pintando los botones del jpanel grande
                switch(cadenaChar[k]){
                    case '0':  
                        Visibles.setVisible(j, i, false);
                        Colores.setColores(j, i, 0);
                        Botones[j][i].setBackground(Color.WHITE);
                        break;
                    case '1': 
                        Visibles.setVisible(j, i, true);
                        Colores.setColores(j, i, 1);
                        Botones[j][i].setBackground(Color.BLACK);
                        break;
                    case '2': 
                        Visibles.setVisible(j, i, true);
                        Colores.setColores(j, i, 2);
                        Botones[j][i].setBackground(Color.RED);
                        break;
                    case '3':   
                        Visibles.setVisible(j, i, true);
                        Colores.setColores(j, i, 3); 
                        Botones[j][i].setBackground(Color.BLUE);
                        break;
                    case '4':
                        Visibles.setVisible(j, i, true);
                        Colores.setColores(j, i, 4); 
                        Botones[j][i].setBackground(Color.GREEN);
                        break;
                    case '5':
                        Visibles.setVisible(j, i, true);
                        Colores.setColores(j, i, 5);  
                        Botones[j][i].setBackground(Color.YELLOW);
                        break;
                    case '6':
                        Visibles.setVisible(j, i, true);
                        Colores.setColores(j, i, 6); 
                        Botones[j][i].setBackground(Color.MAGENTA);
                        break;
                    case '7':
                        Visibles.setVisible(j, i, true);
                        Colores.setColores(j, i, 7); 
                        Botones[j][i].setBackground(Color.ORANGE);
                        break;
                    case '8':
                        Visibles.setVisible(j, i, true);
                        Colores.setColores(j, i, 8); 
                        Botones[j][i].setBackground(Color.CYAN);
                        break;
                    case '9':
                        Visibles.setVisible(j, i, true);
                        Colores.setColores(j, i, 9); 
                        Botones[j][i].setBackground(Color.PINK);
                        break;
                } //fin del switch               
                k++;
            }//fin del for            
        }//fin del for     
    }//fin del método leerDibujo
    
    

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        jButton8 = new javax.swing.JButton();
        jButton9 = new javax.swing.JButton();
        jButton10 = new javax.swing.JButton();
        jButton11 = new javax.swing.JButton();
        jButton12 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem4 = new javax.swing.JMenuItem();
        jMenuItem5 = new javax.swing.JMenuItem();

        setClosable(true);
        setIconifiable(true);
        setTitle("Dibujar");
        setMaximumSize(new java.awt.Dimension(630, 730));
        setMinimumSize(new java.awt.Dimension(630, 730));
        setPreferredSize(new java.awt.Dimension(630, 730));
        addInternalFrameListener(new javax.swing.event.InternalFrameListener() {
            public void internalFrameActivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeactivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeiconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameIconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosed(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosing(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameOpened(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameOpened(evt);
            }
        });

        jPanel1.setMaximumSize(new java.awt.Dimension(600, 600));
        jPanel1.setMinimumSize(new java.awt.Dimension(600, 600));
        jPanel1.setName("jPanel1"); // NOI18N
        jPanel1.setPreferredSize(new java.awt.Dimension(600, 600));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 605, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 605, Short.MAX_VALUE)
        );

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Spin.gif"))); // NOI18N
        jButton1.setText("Guardar");
        jButton1.setName("jbtnGuardar"); // NOI18N
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Rice.gif"))); // NOI18N
        jButton2.setText("Limpiar");
        jButton2.setName("jbtnLimpiar"); // NOI18N
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 204), 2, true));
        jPanel2.setName("jPanel2"); // NOI18N

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 88, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jButton4.setName("jButton4");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        jButton5.setName("jButton5");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        jButton6.setName("jButton6");
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });

        jButton7.setName("jButton7");
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });

        jButton8.setName("jButton8");
        jButton8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton8ActionPerformed(evt);
            }
        });

        jButton9.setName("jButton9");
        jButton9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton9ActionPerformed(evt);
            }
        });

        jButton10.setName("jButton10");
        jButton10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton10ActionPerformed(evt);
            }
        });

        jButton11.setName("jButton11");
        jButton11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton11ActionPerformed(evt);
            }
        });

        jButton12.setName("jButton12");
        jButton12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton12ActionPerformed(evt);
            }
        });

        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Slur.gif"))); // NOI18N
        jButton3.setText("Editar");
        jButton3.setName("jButton3");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jMenuBar1.setName("jMenuBar1"); // NOI18N

        jMenu1.setText("Archivos");
        jMenu1.setName("jMenu1"); // NOI18N
        jMenu1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenu1ActionPerformed(evt);
            }
        });

        jMenuItem1.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_G, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem1.setText("Guardar");
        jMenuItem1.setName("jMenuItem1"); // NOI18N
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem1);

        jMenuItem3.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_X, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem3.setText("Cerrar");
        jMenuItem3.setName("jMenuItem3"); // NOI18N
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem3);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Edición");
        jMenu2.setName("jMenu2"); // NOI18N
        jMenu2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenu2ActionPerformed(evt);
            }
        });

        jMenuItem2.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_L, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem2.setText("Limpiar");
        jMenuItem2.setName("jMenuItem2"); // NOI18N
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem2);

        jMenuItem4.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem4.setText("Colores");
        jMenuItem4.setName("jMenuItem4"); // NOI18N
        jMenuItem4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem4ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem4);

        jMenuItem5.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem5.setText("Editar dibujo");
        jMenuItem5.setName("jMenuItem5");
        jMenuItem5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem5ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem5);

        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jButton1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jButton10, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton11, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton12, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton9, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jButton8, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton4, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton5, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton6, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton7, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 605, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(288, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(11, 11, 11)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                            .addComponent(jButton6, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 17, Short.MAX_VALUE)
                                            .addComponent(jButton4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(jButton8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(jButton5, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jButton11, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jButton10, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jButton12, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jButton9, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addComponent(jButton7, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, 40, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jButton2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jButton3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 605, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(35, 35, 35))
        );

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jButton2.getAccessibleContext().setAccessibleName("Nueva Partida");

        pack();
    }// </editor-fold>//GEN-END:initComponents
   
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed

        guardarDibujo(); 
    }//GEN-LAST:event_jButton1ActionPerformed

    private void formInternalFrameOpened(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameOpened
        ponerBotones();
        nuevaPartida();   
    }//GEN-LAST:event_formInternalFrameOpened

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        nuevaPartida();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jMenu1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenu1ActionPerformed
        guardarDibujo();  
    }//GEN-LAST:event_jMenu1ActionPerformed

    private void jMenu2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenu2ActionPerformed
        nuevaPartida();
    }//GEN-LAST:event_jMenu2ActionPerformed

    private void jMenuItem4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem4ActionPerformed
        Object seleccion = JOptionPane.showInputDialog(null, "Seleccione opcion","Selector de opciones",
                JOptionPane.QUESTION_MESSAGE, null,  // null para icono defecto
                new Object[] { "Negro", "Rojo", "Azul", "Verde", "Amarillo", "Magenta", "Naranja", "Cyan", "Rosa" },
                "Negro");
        
        if (seleccion == "Negro"){
            elegirColor("Negro");
        }
        
        if (seleccion == "Rojo"){
           elegirColor("Rojo");
        }
        
        if (seleccion == "Azul"){
            elegirColor("Azul");
        }
        
        if (seleccion == "Verde"){
            elegirColor("Verde");
        }
        
        if (seleccion == "Amarillo"){
           elegirColor("Amarillo");
        }    
        
        if (seleccion == "Magenta"){
           elegirColor("Magenta");
        }   
        
        if (seleccion == "Naranja"){
           elegirColor("Naranja");
        }  
        
        if (seleccion == "Cyan"){
           elegirColor("Cyan");
        }    
        
        if (seleccion == "Rosa"){
           elegirColor("Rosa");
        }    
    }//GEN-LAST:event_jMenuItem4ActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        this.dispose();
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        nuevaPartida();
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        guardarDibujo();
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jButton8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton8ActionPerformed
        elegirColor("Negro");
    }//GEN-LAST:event_jButton8ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        elegirColor("Rojo");
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        elegirColor("Azul");
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        elegirColor("Verde");
    }//GEN-LAST:event_jButton6ActionPerformed

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
        elegirColor("Amarillo");
    }//GEN-LAST:event_jButton7ActionPerformed

    private void jButton10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton10ActionPerformed
        elegirColor("Naranja");
    }//GEN-LAST:event_jButton10ActionPerformed

    private void jButton11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton11ActionPerformed
        elegirColor("Cyan");
    }//GEN-LAST:event_jButton11ActionPerformed

    private void jButton12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton12ActionPerformed
        elegirColor("Rosa");
    }//GEN-LAST:event_jButton12ActionPerformed

    private void jButton9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton9ActionPerformed
        elegirColor("Magenta");
    }//GEN-LAST:event_jButton9ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        leerDibujo();
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jMenuItem5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem5ActionPerformed
        leerDibujo();
    }//GEN-LAST:event_jMenuItem5ActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton10;
    private javax.swing.JButton jButton11;
    private javax.swing.JButton jButton12;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JButton jButton8;
    private javax.swing.JButton jButton9;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItem5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    // End of variables declaration//GEN-END:variables

    
}
